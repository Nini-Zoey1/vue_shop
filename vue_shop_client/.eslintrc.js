module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'semi': 0,
    "no-multiple-empty-lines": [0, { "max": 100 }],
    'no-trailing-spaces': 0,
    'padded-blocks': 0,
    'space-before-function-paren': 0,
    'vue/no-unused-vars': 0,
    'no-unused-vars': 0
  }
}
